class Hamburger {
  constructor({
    size,
    stuffing
  }) {
    this._size = size;
    this._stuffing = stuffing;
    this._toppings = [];
  };

  static SIZE_SMALL = 'SIZE_SMALL';
  static SIZE_LARGE = 'SIZE_LARGE';
  static SIZES = {
    [Hamburger.SIZE_SMALL]: {
      price: 30,
      calories: 50,
    },
    [Hamburger.SIZE_LARGE]: {
      price: 50,
      calories: 100,
    },
  };

  static STUFFING_CHEESE = 'STUFFING_CHEESE';
  static STUFFING_SALAD = 'STUFFING_SALAD';
  static STUFFING_MEAT = 'STUFFING_MEAT';

  static STUFFINGS = {
    [Hamburger.STUFFING_CHEESE]: {
      price: 15,
      calories: 20,
    },
    [Hamburger.STUFFING_SALAD]: {
      price: 20,
      calories: 5,
    },
    [Hamburger.STUFFING_MEAT]: {
      price: 35,
      calories: 15,
    },
  };

  static TOPPING_SPICE = 'TOPPING_SPICE';
  static TOPPING_SAUCE = 'TOPPING_SAUCE';

  static TOPPINGS = {
    [Hamburger.TOPPING_SPICE]: {
      price: 10,
      calories: 0,
    },
    [Hamburger.TOPPING_SAUCE]: {
      price: 15,
      calories: 5,
    },
  };

  addTopping(topping) {
    if (this._toppings.length > 0) {
      this._toppings.includes(topping) ? console.log(`you want add ${topping} his we Have`) : this._toppings.push(topping);
    }

    if (this._toppings.length === 0) {
      this._toppings.push(topping);
    }
  }

  removeTopping(topping) {
    this._toppings = this._toppings.filter(key => key !== topping);
  }

  get toppings() {
    return this._toppings;
  }

  get size() {
    return this._size;
  }

  get stuffing() {
    return this._stuffing;
  }

  get calculatePrice() {
    const getSizePrice = Hamburger.SIZES[this._size].price;
    const getStuffingPrice = Hamburger.STUFFINGS[this._stuffing].price;
    const getToppingsPrice = this._toppings.reduce((acc, value) => acc + Hamburger.TOPPINGS[value].price, 0);

    const totalPrice =  getToppingsPrice + getSizePrice + getStuffingPrice;

    return totalPrice;
  }

  get calculateCalories() {
    const getSizeCalories = Hamburger.SIZES[this._size].calories;
    const getStuffingCalories = Hamburger.STUFFINGS[this._stuffing].calories;
    const getToppingsCalories = this._toppings.reduce((acc, value) => acc + Hamburger.TOPPINGS[value].calories, 0);

    const totalCalories =  getToppingsCalories + getSizeCalories + getStuffingCalories;

    return totalCalories;
  }
}

const hamburger = new Hamburger({
  size: Hamburger.SIZE_SMALL,
  stuffing: Hamburger.STUFFING_CHEESE
});

// Добавка из приправы
hamburger.addTopping(Hamburger.TOPPING_SPICE);

// Спросим сколько там калорий
console.log("Calories: ", hamburger.calculateCalories);

// Сколько стоит?
console.log("Price: ", hamburger.calculatePrice);

// Я тут передумал и решил добавить еще соус
hamburger.addTopping(Hamburger.TOPPING_SAUCE);

// А сколько теперь стоит?
console.log("Price with sauce: ", hamburger.calculatePrice);

// Проверить, большой ли гамбургер?
console.log("Is hamburger large: ", hamburger.getSize === Hamburger.SIZE_LARGE); // -> false

// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SAUCE);

// Смотрим сколько добавок
console.log("Hamburger has %d toppings", hamburger.toppings.length); // 1
